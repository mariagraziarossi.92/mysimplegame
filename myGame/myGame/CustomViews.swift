//
//  CustomViews.swift
//  myGame
//
//  Created by Mariagrazia Rossi on 03/08/21.
//

import UIKit

protocol CellProtocol {
    func pressedCell(row: Int, col: Int)
}

class CellView: UIView {
    var row: Int = 9999
    var column: Int = 9999
    var score: Int = 0
    
    var isBlue: Bool {
        get {
            return self.backgroundColor == .blue
        }
        set{
            self.backgroundColor = newValue ? .blue : .gray
        }
        
    }
    
    var delegate: CellProtocol?
    
    @IBAction func pressCell(){
        if !self.isBlue {
            delegate?.pressedCell(row: row, col: column)
        }
    }
    
    func configure(row: Int, col: Int) {
        self.row = row
        self.column = col
        isBlue = false
    }
    
    func setBlue() {
        self.isBlue = true
    }
    
    func setScore(_ v: Int){
        self.score = v
    }
}

class ColumnView: UIStackView {
    var cellArray: [CellView?] {
        var cells: [CellView?] = []
        self.subviews.forEach({
            if let x = $0 as? CellView {
                cells.append(x)
            }
            
        })
        return cells
    }
    
    var lastBlueCellRow: Int {
        guard let c = cellArray.first(where: {$0?.isBlue == true}) else {return lenght}
        return c?.row ?? lenght
    }
    
    var firstGrayCellRow: Int {
        guard let c = cellArray.last(where: {$0?.isBlue == false}) else {return lenght}
        return c?.row ?? 0
    }
    
    var numOfBlueCell: Int {
        return cellArray.filter({$0?.isBlue == true}).count
    }
    
    var numOfGreyCell: Int {
        return cellArray.filter({$0?.isBlue == false}).count
    }
    
    var lenght: Int {
        return cellArray.count
    }
    
    func getScore() -> Int {
        var score = 0
        for i in 0...numOfBlueCell {
            score += 5*i
        }
        
        if lastBlueCellRow < firstGrayCellRow {
            score += (lenght - lastBlueCellRow - numOfBlueCell)*10
        }
        
        return score
    }
}

class GridView: UIStackView {
    var columnArray: [ColumnView?] {
        var cells: [ColumnView?] = []
        self.subviews.forEach({
            if let x = $0 as? ColumnView {
                cells.append(x)
            }
        })
        return cells
    }
    
    var lenght: Int {
        return columnArray.count
    }
    
    func getBridgePosition(in col: Int, from row: Int) -> Int? {
        guard let c = columnArray[col], col+1 < lenght, col-1 >= 0,
              let rCol = columnArray[col+1], let lCol = columnArray[col-1],
              let selRow = c.cellArray.firstIndex(where: {lCol.cellArray[$0!.row]?.isBlue == true && rCol.cellArray[$0!.row]?.isBlue == true && lCol.lastBlueCellRow >= row && rCol.lastBlueCellRow >= row}), c.cellArray[selRow]?.isBlue == false else {
            return nil
        }
        return selRow
    }
    
    func getDropDownPosition(in col: Int, from row: Int) -> Int? {
        guard let column = columnArray[col], column.numOfBlueCell < column.lenght else {return nil}
        if column.lastBlueCellRow > row {
            return (column.lastBlueCellRow)-1
        } else {
            return column.firstGrayCellRow
        }
    }
    
    func getCellFinalPosition(in col: Int, from row: Int) -> Int? {
        return getBridgePosition(in: col, from: row) ?? getDropDownPosition(in: col, from: row)
    }
    
    func getFinalCell(in col: Int, from row: Int) -> CellView? {
        guard let pos = getCellFinalPosition(in: col, from: row), let c = columnArray[col] else {return nil}
        return c.cellArray[pos]
    }
    
    func getTotalScore() -> Int {
        var score = 0
        columnArray.forEach({ col in
            if let col = col, col.numOfBlueCell>0 {
                score += col.getScore()
            }
        })
        return score
    }

    func startAnimationBlock(in col: Int, from startRow: Int, with completion: @escaping ()->()){
        
        if let column = self.columnArray[col], let pressedCell = column.cellArray[startRow], let endCell = getFinalCell(in: col, from: startRow)  {
            self.isUserInteractionEnabled = false
            
            let block = UIView(frame: CGRect(origin: pressedCell.frame.origin, size: pressedCell.frame.size))
            block.backgroundColor = .blue
            column.addSubview(block)
            block.center = pressedCell.center
            
            animateContainer(view: block , delay: 0, to: endCell.center, completion: {
                self.isUserInteractionEnabled = true
                endCell.setBlue()
                completion()
            })
        }
    }
    
    private func animateContainer(view: UIView, delay: Double, to: CGPoint, completion: @escaping ()->()) {
        UIView.animate(withDuration: 0.3, delay: delay, options: [.curveEaseInOut], animations: { [weak view] in
            guard let view = view else { return }
            view.center.y = to.y
        }, completion: {_ in
            view.removeFromSuperview()
            completion()})
    }
}
