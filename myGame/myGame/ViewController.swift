//
//  ViewController.swift
//  myGame
//
//  Created by Mariagrazia Rossi on 30/07/21.
//

import UIKit

class GameViewController: UIViewController {

    @IBOutlet weak var horizontalStackView: GridView!
    @IBOutlet weak var scoreLabel: UILabel!
    @IBOutlet weak var restart: UIButton!
    
    var countBlueBlock: Int = 0 {
        didSet {
            if countBlueBlock == 10 {
                showEndGamePopup()
            }
        }
    }
    
    var score: Int = 0 {
        didSet {
            scoreLabel.text = "Score: \(score)"
        }
    }
    
    @IBAction func pressRestart(_ sender: Any) {
        initGame()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        initGame()
    }
    
    func initGame() {
        countBlueBlock = 0
        score = 0
        
        for col in 0...horizontalStackView.columnArray.count-1 {
            if let vStackView = horizontalStackView.columnArray[col] {
                for row in 0...vStackView.cellArray.count-1 {
                    if let cell = vStackView.cellArray[row] {
                        cell.configure(row: row, col: col)
                        cell.delegate = self
                    }
                }
            }
        }
    }
    
    func showEndGamePopup(){
        let alert = UIAlertController(title: "Finish!", message: "You get \(score) points", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        alert.addAction(UIAlertAction(title: "Restart", style: .cancel, handler: { _ in self.pressRestart(self)
          }))
        present(alert, animated: true, completion: nil)
    }
    
    func updateScoreAndCounter() {
        self.score =  self.horizontalStackView.getTotalScore()
        self.countBlueBlock+=1
    }
}

extension GameViewController: CellProtocol {
    func pressedCell(row: Int, col: Int) {
        
        guard countBlueBlock < 10 else {return}
        
        horizontalStackView.startAnimationBlock(in: col, from: row, with: self.updateScoreAndCounter)
    }
        
}
